import os
# Run a test server.
from webapp import app
app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 5000)), debug=True)