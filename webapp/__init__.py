# Import flask and template operators
from flask import Flask, render_template, session
from flask_oauthlib.client import OAuth, OAuthException
from flask_sqlalchemy import SQLAlchemy

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)

oauth = OAuth(app)

spotify = oauth.remote_app(
    'spotify',
    consumer_key='ff290308d4fa4cc9b7c929ee682ab061',
    consumer_secret='da031312410b4018ba469a534b192273',
    base_url='https://api.spotify.com/',
    request_token_url=None,
    access_token_url='https://accounts.spotify.com/api/token',
    authorize_url='https://accounts.spotify.com/authorize'
)

@spotify.tokengetter
def get_spotify_oauth_token():
    return session.get('oauth_token')

from webapp.controllers import controller, profile, seedsong, parties

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

db.create_all()