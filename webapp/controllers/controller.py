from webapp import app, spotify, db
from flask import session, url_for, request, redirect, render_template
from flask_oauthlib.client import OAuthException
from sqlalchemy import inspect
import pandas as pd
import json

@app.route('/tables')
def show_tables():
    inspector = inspect(db.engine)
    for table_name in inspector.get_table_names():
        for column in inspector.get_columns(table_name):
            print("Table: %s Column: %s" % (table_name, column['name']))

@app.route('/saveplaylist')
def save_playlist():
    library = pd.read_sql_query("select * from `playlists" + session["partyid"] + "` where partyid == '" + session["partyid"] + "'", db.engine)
    uris = library["uri"]
    url = '/v1/users/' + session["user"] + '/playlists'
    data = {"name": "DSC/e Collaborative Playlist",
            "public": "false",
            "collaborative": "false",
            "description" : "A playlist created through group-playlists.markgraus.net, from the users " + ",".join(library["userid"].unique())}
    print(json.dumps(data))
    playlistresp = spotify.request(url, data=data, format="json",method="POST")
    print(playlistresp.data)
    if playlistresp.status == 200 or playlistresp.status == 201:
        playlistid = playlistresp.data["id"]
        url = "/v1/users/" + session["user"] + "/playlists/" + playlistid + "/tracks"
        data = {"uris" : uris.tolist()}
        spotify.request(url, data=data, format="json",method="POST")
        return "ok"
    else:
        return "error"

@app.route('/tospotify')
def to_spotify():
    library = pd.read_sql_query("select * from `playlists" + session["partyid"] + "` where partyid == '" + session["partyid"] + "'", db.engine)
    uris = library["uri"]
    if request.args.get("player"):
        url = '/v1/me/player/play?device_id=' + request.args.get("player")
    else:
        url = '/v1/me/player/play'
    data = {"uris": uris.tolist()}
    print(data)
    playlistresp = spotify.request(url, data=data, format="json",method="PUT")
    print(playlistresp.data)
    if playlistresp.status == 200 or playlistresp.status == 201:
        return "ok"
    else:
        return "error"

@app.route('/')
def index():
    if "oauth_token" not in session:
        callback = url_for(
            'authorized',
            _external=True
        )
        #REDIRECT TO callback URL
        return spotify.authorize(callback=callback,
                                 scope="user-top-read user-read-playback-state user-modify-playback-state user-read-recently-played playlist-modify-private")
    else:
        if "scraped" in session:
            if "party" in session:
                return redirect(url_for("show_party", partyid=session["partyid"]))
            else:
                return redirect(url_for("show_parties"))
        else:
            return render_template('index.html')

@app.route('/seedsong')
def seedsong_interface():
    seedsongs = pd.read_sql_query("select seedsong,track,artist from parties where partyid == '" + session["partyid"] + "'", db.engine)
    seedsongs = seedsongs[["seedsong","track","artist"]].drop_duplicates().set_index("seedsong").to_json(orient="index")
    playlist = pd.DataFrame()
    if "playlists"+session["partyid"] in db.engine.table_names():
        playlist = pd.read_sql_query("select * from `playlists" +session["partyid"] + "` where partyid == '" + session["partyid"] + "'", db.engine)
    return render_template('seedsong-interface.html', seedsongs={"seedsongs":seedsongs, "playlist":playlist})

@app.route('/logout')
def logout():
    del(session["oauth_token"])
    del(session["user"])
    return redirect(url_for('index'))

@app.route('/login/authorized')
def authorized():
    resp = spotify.authorized_response()
    if resp is None:
        return 'Access denied: reason={0} error={1}'.format(
            request.args['error_reason'],
            request.args['error_description']
        )
    if isinstance(resp, OAuthException):
        return 'Access denied: {0}'.format(resp.message)
    session['oauth_token'] = (resp['access_token'], '')
    me = spotify.request('/v1/me/')
    if me.status != 200:
        return redirect(url_for("logout"))
    else:
        session["user"] = me.data["id"]
        if me.data["display_name"] == None:
            display_name = ""
        else:
            display_name = me.data["display_name"]
        if len(me.data["images"]) == 0:
            image_url = ""
        else:
            image_url = me.data["images"][0]["url"]
        users = pd.DataFrame({
            "userid":session["user"],
            "display_name": display_name,
            "image_url": image_url
        }, index=[""])
        print(users)
        users.to_sql("users", db.engine, if_exists="append", index=False)
        return redirect(url_for('show_scrape'))


@app.route("/library")
def show_library():
    returnlibrary = [pd.DataFrame()]
    if "tracks" in db.engine.table_names():
        if "partyid" in session:
            query = "select * from tracks inner join toptracks on toptracks.trackid = tracks.id where userid in (select userid from parties where partyid == '" + session["partyid"] + "')"
            print(query)
            library = pd.read_sql_query(query, db.engine)
        else:
            library = pd.read_sql_query("select * from tracks where id in (select trackid from toptracks where userid == '" + session["user"] + "')",db.engine)
        returnlibrary = []
        for i in library["userid"].unique():
            if len(library.loc[library["userid"] == i].index) > 300:
                returnlibrary.append(library.loc[library["userid"] == i].sample(300))
            else:
                returnlibrary.append(library.loc[library["userid"] == i])
    return pd.concat(returnlibrary).to_csv()
