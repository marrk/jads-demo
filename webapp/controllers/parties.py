from webapp import app, db, spotify
from flask import session, render_template, request, redirect, url_for
import pandas as pd
import numpy as np

@app.route('/party', methods=["GET","POST"])
def show_parties():
    print(request.form)
    print(request.args)
    if "action" in request.form:
        if request.form["action"] == "new":
            party = pd.DataFrame({"partyid":[request.form["partyname"]], "userid":[session["user"]], "seedsong":[""], "track":[""],"artist":[""]})
            party.to_sql("parties", db.engine, if_exists="append", index=False)
            session["partyid"] = request.form["partyname"]
            return redirect(url_for("show_party", partyid=session["partyid"]))
    elif "action" in request.args:
        if request.args.get("action") == "join":
            parties = pd.read_sql_query("select partyid,userid from parties where userid == '" + session["user"] + "' and partyid == '" + request.args.get("partyname") + "'", db.engine)
            if len(parties.index) == 0:
                party = pd.DataFrame({"partyid": [request.args.get("partyname")], "userid": [session["user"]], "seedsong": [""], "track":[""],"artist":[""]})
                party.to_sql("parties", db.engine, if_exists="append", index=False)
            session["partyid"] = request.args.get("partyname")
            return redirect(url_for("show_party", partyid=session["partyid"]))
    else:
        returnparties = {}
        if "parties" in db.engine.table_names():
            parties = pd.read_sql_query("select DISTINCT partyid,userid from parties", db.engine)
            for index, party in parties.iterrows():
                if party["partyid"] in returnparties:
                    returnparties[party["partyid"]].append(party["userid"])
                else:
                    returnparties[party["partyid"]] = [party["userid"]]
        print(returnparties)
        return render_template('parties.html', parties=returnparties)

@app.route('/playlists/<partyid>', methods=['GET','POST'])
def calculate_playlists(partyid):
    playlist = pd.DataFrame()
    #no-interaction
    rows = pd.read_sql_query("select * from toptracks left join tracks on (tracks.id == toptracks.trackid) where userid in (select userid from parties where partyid == '"+ session['partyid'] + "')", db.engine)
    features = ["popularity", "acousticness", "danceability", "energy", "instrumentalness", "liveness", "speechiness",
                "valence"]
    points = rows.groupby("userid", as_index=False)[features].agg(["median"])
    for user in rows["userid"].unique():
        rows.loc[rows["userid"] != user].map()
    print(points)
    baselineplaylist = rows
    #average
    #seedsong-recommendations
    seedsongs = pd.read_sql_query("select seedsong,userid from parties where partyid == '" + session["partyid"] + "' and seedsong <> ''", db.engine)
    for index,row in seedsongs.iterrows():
        url = '/v1/recommendations?seed_tracks=' + row['seedsong'] + "&limit=50"
        print(url)
        recommendationsrequest = spotify.request(url)
        print(recommendationsrequest.data)
        subdf = pd.io.json.json_normalize(recommendationsrequest.data["tracks"])
        subdf["userid"] = row["userid"]
        recommendations.append(subdf)
    tracks = pd.concat(recommendations)
    playlist = tracks.sample(25)
    playlist["imageurl"] = playlist["album.images"].map(lambda x: x[2]["url"])
    playlist["artist"] = playlist["artists"].map(lambda x: x[0]["name"])
    playlist["partyid"] = session["partyid"]
    playlist[["uri","imageurl","artist","partyid","userid","name"]].to_sql("playlists"+session["partyid"], db.engine, if_exists="replace", index=False)
    #playersrequest = spotify.request('/v1/me/player/devices')
    #print(playersrequest.data)
    #if "devices" in playersrequest.data:
    #    players = playersrequest.data["devices"]
    #else:
    #    players = []
    return redirect(url_for("seedsong"))


@app.route('/playlist')
def showplaylist():
    playlist = pd.DataFrame()
    if "playlists"+session["partyid"] in db.engine.table_names():
        playlist = pd.read_sql_query("select * from `playlists" +session["partyid"] + "` where partyid == '" + session["partyid"] + "'", db.engine)
    return playlist.sort_values("id").to_json(orient="records")

@app.route('/party/<partyid>', methods=['GET', "POST"])
def show_party(partyid):
    playlist = pd.DataFrame()
    if "playlists"+session["partyid"] in db.engine.table_names():
        rows = pd.read_sql_query("select * from `playlists" +session["partyid"] + "` where partyid == '" + session["partyid"] + "'", db.engine)
        if len(rows.index) != 0:
            playlist = rows
            print("found")
    if len(playlist.index) == 0 or request.args.get("action",False) == "recalculate":
        seedsongs = pd.read_sql_query("select seedsong,userid from parties where partyid == '" + session["partyid"] + "' and seedsong <> '' and userid == '" + session["user"] + "'", db.engine)
        if len(seedsongs.index) == 0:
            return redirect(url_for("seedsong_interface"))
        else:
            recommendations = []
            seedsongs = pd.read_sql_query("select seedsong,userid from parties where partyid == '" + session["partyid"] + "' and seedsong <> ''", db.engine)
            for index,row in seedsongs.iterrows():
                url = '/v1/recommendations?seed_tracks=' + row['seedsong'] + "&limit=50"
                print(url)
                recommendationsrequest = spotify.request(url)
                print(recommendationsrequest.data)
                subdf = pd.io.json.json_normalize(recommendationsrequest.data["tracks"])
                subdf["userid"] = row["userid"]
                recommendations.append(subdf)
            tracks = pd.concat(recommendations)
        playlist = tracks.sample(25)
        playlist["imageurl"] = playlist["album.images"].map(lambda x: x[2]["url"])
        playlist["artist"] = playlist["artists"].map(lambda x: x[0]["name"])
        playlist["partyid"] = session["partyid"]
        playlist[["uri","imageurl","artist","partyid","userid","name"]].to_sql("playlists"+session["partyid"], db.engine, if_exists="replace", index=False)
    #playersrequest = spotify.request('/v1/me/player/devices')
    #print(playersrequest.data)
    #if "devices" in playersrequest.data:
    #        players = playersrequest.data["devices"]
    #    else:
    #    players = []
    #return render_template("playlist.html", playlist={"playlist":playlist,"players":players})
    return redirect(url_for("seedsong_interface"))
