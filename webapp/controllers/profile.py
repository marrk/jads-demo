from webapp import app, db, spotify
from flask import session, render_template, send_file, redirect, url_for
import pandas as pd
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
from io import BytesIO

#s3 = boto3.client('s3')
#S3_BUCKET = os.environ.get('S3_BUCKET')

#q = Queue(connection=conn)



@app.route("/me")
def show_me():
    user = session['user']
    toptracks = pd.read_sql_query("select * from tracks join toptracks where userid == '" + user + "' limit 0,10", db.engine)
    userinfo = pd.read_sql_query("select * from users where userid == '" + user + "'", db.engine)
    if len(userinfo.index) == 0 or len(toptracks.index) == 0:
      return redirect(url_for('scrape_top_tracks'))
    else:
      me = {
          "userinfo":userinfo.to_dict("records"),
          "toptracks":toptracks.to_dict("records")
      }
      print(me)
      return render_template("profile.html", profile=me)

@app.route('/scrape')
def show_scrape():
    me = spotify.request('/v1/me/')
    session["user"] = me.data["id"]
    if me.status == 200:
      if me.data["display_name"] == None:
        display_name = ""
      else:
        display_name = me.data["display_name"]
      if len(me.data["images"]) == 0:
        image_url = ""
      else:
        image_url = me.data["images"][0]["url"]
      #db.execute("INSERT INTO users VALUES (?,?,?)", (me.data["id"],display_name, image_url))
      #db.commit()
    #session["library"] = library.to_json()
    users = pd.DataFrame({
        "userid":session["user"],
        "display_name": display_name,
        "image_url": image_url
    }, index=[""])
    users.to_sql("users", db.engine, if_exists="append", index=False)
    url = '/v1/me/top/tracks?limit=1000&time_range=short_term'
    done = False
    lists = []
    while not done:
        print(url)
        recommendations = spotify.request(url)
        #print(recommendations.data)x
        print(recommendations.data)
        if recommendations.status == 200:
            tracks = pd.io.json.json_normalize(recommendations.data["items"])
            lists.append(tracks)
            if (not recommendations.data["next"]) or (len(recommendations.data["items"]) == 0):
                done = True
            else:
                url = recommendations.data["next"]
    library = pd.concat(lists)
    trackfeatures = pd.DataFrame()
    if "id" in library.columns.values:
        for i in range(0, len(library["id"].values), 100):
            url = "https://api.spotify.com/v1/audio-features?ids=" + ",".join(library["id"].values[i:i + 100])
            audiofeaturesrequest = spotify.request(url)
            featuredata = audiofeaturesrequest.data
            trackfeatures = trackfeatures.append(pd.io.json.json_normalize([x for x in featuredata["audio_features"] if x is not None]))
    #drop(["album.images", "album.href","external_ids.isrc","external_urls.spotify","href","linked_from.external_urls.spotify","linked_from.href","linked_from.id","linked_from.type","linked_from.uri"], axis=1)
    #print(library[0:3])
        toptracks = pd.DataFrame()
        toptracks["trackid"] = library["id"]
        toptracks["userid"] = session["user"]
        library["firstartist"] = library["artists"].map(lambda x: x[0]["name"])
        library["imageurl"] = library["album.images"].map(lambda x: x[2]["url"])
        library = library[["name","popularity","preview_url","track_number","id","firstartist","imageurl"]].merge(trackfeatures.drop(["analysis_url","track_href","uri"], axis=1), how="left", left_on="id", right_on="id")
        ##SQL PART
        library.to_sql("tracks",db.engine,if_exists="append", index=False)
        toptracks.to_sql("toptracks", db.engine, if_exists="append", index=False)
    session["scraped"] = True
    ##FTP part
    # ftp = ftplib.FTP('ftp.cluster003.hosting.ovh.net', 'markgrauzc', 'Viezehenk01')
    # tracksfile = BytesIO()
    # text_file = TextIOWrapper(tracksfile)
    # text_file.write(toptracks.to_csv(line_terminator='\r\n', encoding = "ISO-8859-1"))
    # text_file.seek(0)
    # ftp.storbinary('STOR /group-playlists/' + session["user"] + '_toptracks.csv', tracksfile)  # send the file
    # libraryfile = BytesIO()
    # text_file2 = TextIOWrapper(tracksfile, encoding = "ISO-8859-1")
    # text_file2.write(library.to_csv(line_terminator='\r\n', encoding = "ISO-8859-1"))
    # libraryfile.seek(0)
    # ftp.storbinary('STOR /group-playlists/' + session["user"] + '_library.csv', libraryfile)  # send the file
    # ftp.quit()

    ##S3 part
    #s3.get_bucket(os.environ.get("S3_BUCKET","group-playlist-heroku-s3")).put_object(Key=session["user"] + '_library.csv', Body=library.to_csv())
    #s3.get_bucket(os.environ.get("S3_BUCKET","group-playlist-heroku-s3")).put_object(Key=session["user"] + '_toptracks.csv', Body=toptracks.to_csv())
    return redirect(url_for('index'))

@app.route('/evplot.png')
def evplot():
    library = pd.read_sql_query("select * from tracks where id in (select trackid from toptracks where userid == '" + session["user"] + "')", db.engine)
    print(len(library.index))
    #library = pd.read_sql("select * from library",get_db())
    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)
    xs = library["valence"]
    xs = xs.loc[xs.notnull()]
    ys = library["energy"]
    ys = ys.loc[ys.notnull()]
    heatmap, xedges, yedges = np.histogram2d(xs, ys, bins=50)
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
    axis.imshow(heatmap.T, extent=extent, origin='lower')
    axis.set_xlabel("Valence")
    axis.set_ylabel("Energy")
    canvas = FigureCanvas(fig)
    img = BytesIO()
    fig.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@app.route('/featureplots.png')
def featureplot():
  features = ["popularity","acousticness","danceability","energy","instrumentalness","liveness","speechiness","valence"]
  usertracks = pd.read_sql_query("select * from tracks where id in (select trackid from toptracks where userid == '" + session["user"] + "')",db.engine)
  library = pd.read_sql_query("select * from tracks", db.engine)
  fig = Figure(figsize=(14, 4), dpi=100)
  for i in range(0,len(features)):
    axis = fig.add_subplot(1, len(features), i+1)
    x = usertracks[features[i]]
    x = x.loc[x.notnull()]
    all = library[features[i]]
    all = all.loc[all.notnull()]
    if features[i] == "popularity":
      bins = np.linspace(0,100,40)
    else:
      bins = np.linspace(0, 1, 40)
    axis.hist(x, bins, alpha=0.5, label="you")
    axis.hist(all, bins, alpha=0.5, label="everybody")
    axis.set_xlabel(features[i])
  canvas = FigureCanvas(fig)
  img = BytesIO()
  fig.savefig(img)
  img.seek(0)
  return send_file(img, mimetype='image/png')
  
@app.route("/db")
def return_db():
  export = pd.read_sql_query("SELECT * FROM toptracks INNER JOIN tracks ON toptracks.trackid == tracks.id", db.engine).to_csv(line_terminator='\r\n',encoding="utf-8")
  return export