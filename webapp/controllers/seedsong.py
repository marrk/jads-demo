from webapp import app, db, spotify
from flask import session, request
import pandas as pd
import json
#
#@app.route("/sliders")
#def show_sliders():
#  return render_template("seedsongs.html")

#
#@app.route("/seedsong-pca",methods=["POST"])
#def show_pca():
#  return render_template("seedsongs.html")

#
#@app.route("/return-seedsong-set-sliders",methods=["POST"])
#def seedsong_set_sliders():
#  print("hallo")
#  print(request.form)
#  library = pd.read_sql_query("select * from tracks where id in (select trackid from toptracks where userid == '" + session["user"] + "')", db.engine)
#  library["dist"] = 0
#  for i in request.form:
#    print(i)
#    if i != "popularity":
#      library["dist"] = library["dist"] + library[i].map(lambda x: abs(x-float(request.form[i])/100))
#    else:
#      library["dist"] = library["dist"] + library[i].map(lambda x: abs(x-int(request.form[i])))
#  return library.sort_values("dist").head().to_json(orient="records")


@app.route("/return-seedsong-set-pca",methods=["POST"])
def seedsong_set_pca():
  return {}

@app.route("/addseedsong", methods=["POST"])
def add_seedsong():
  seedsongs = pd.DataFrame({"seedsong":[request.form["track"]], "artist":[request.form["artist"]], "track":[request.form["name"]], "userid":session["user"], "partyid":session["partyid"]})
  seedsongs.to_sql("parties",db.engine,if_exists="append",index=False)
  seedsongs = pd.read_sql_query("select * from (select * from parties where partyid == '" + session["partyid"] + "' and seedsong <> '') as a left join tracks on (a.seedsong == tracks.id)", db.engine)
  recommendations = []
  for index, row in seedsongs.iterrows():
    url = '/v1/recommendations?seed_tracks=' + row['seedsong'] + "&limit=50"
    print(url)
    recommendationsrequest = spotify.request(url)
    subdf = pd.io.json.json_normalize(recommendationsrequest.data["tracks"])
    subdf["userid"] = row["userid"]
    recommendations.append(subdf)
  tracks = pd.concat(recommendations)
  playlist = tracks.sample(25)
  playlist["imageurl"] = playlist["album.images"].map(lambda x: x[2]["url"])
  playlist["artist"] = playlist["artists"].map(lambda x: x[0]["name"])
  playlist["partyid"] = session["partyid"]
  url = "https://api.spotify.com/v1/audio-features?ids=" + ",".join(playlist["id"].values)
  audiofeaturesrequest = spotify.request(url)
  featuredata = audiofeaturesrequest.data
  trackfeatures = pd.io.json.json_normalize([x for x in featuredata["audio_features"] if x is not None])
  playlist = playlist[["imageurl", "artist", "partyid", "userid", "name","id","preview_url"]].merge(trackfeatures.drop(["analysis_url", "track_href"], axis=1), how="left",left_on="id",right_on="id")
  if "playlists"+session["partyid"] in db.engine.table_names():
    oldplaylist = pd.read_sql_query("select * from playlists" + session["partyid"], db.engine)
    playlist = pd.concat([oldplaylist.sample(13), playlist.sample(12)])
  playlist.to_sql("playlists" + session["partyid"],db.engine, if_exists="replace",index=False)
  return json.dumps({"seedsongs" : json.loads(seedsongs.to_json(orient="records")), "playlist": json.loads(playlist.sort_values("id").set_index("id").to_json(orient="records"))})
